/*
JSON Objects
	-JSON stands for JavaScript Object Notation
	-JSON can also be used in other programming languages
	-Do not confuse Javascript objects with JSON

	JSON objects
	-JSON is used for serializing different data types into bytes
		-serialization - process of converting data types into series of bytes for easier transmission or transfer of information
		-bytes - are information that computer processes to perform different tasks

	-Uses double quotes for property name
	-Syntax:

		"propertyA" : "valueA",
		"propertyB" : "valueB"
		
*/

// JSON Objects

/*	{
		"city" : "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	}*/

// JSON Arrays

/*	"cities" : [
		{
			"city:" : "Quezon City",
			"province" : "Metro Manila",
			"country" : "Philippines"
		},
		{
			"city" : "Cebu City",
			"province" : "Cebu",
			"country" : "Philippines"
		}
	]	*/

// JSON Methods
	// JSON objects contain methods for parsing and converting data into stringified JSON

	let batchesArr = [
	{batchName: "Batch 169"},
	{batchName: "Batch 170"}
	];

	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name: "Luke",
		age: 45,
		address: {
			city: "Manila",
			country: "Philippines"
		}
	});

	console.log(data);

	// Using Stringify method with variables

	// User details

	let firstName = prompt("First Name: ");
	let lastName = prompt("Last Name: ");
	let age = prompt("Age: ");
	let address = {
		city: prompt("City: "),
		country: prompt("Country: ")
	}
	
	let data2 = JSON.stringify({
		firstName : firstName,
		lastName : lastName,
		age : age,
		address : address
	});

	console.log(data2);

	// Converting Stringified JSON into Javascript Objects
	
		// JSON.stringify - turn it into JSON
		// JSON.parse - JSON into Javascript Objects

	let batchesJson = `[
		{
			"batchName" : "Batch 169"
		},
		{
			"batchName" : "Batch 170"
		}
	]`

	console.log(JSON.parse(batchesJson));